<?php


namespace App\Services;


use App\Http\Requests\BookRequest;
use App\Http\Resources\BookResource;
use App\Models\Book;
use App\Repositories\Contracts\BookRepositoryInterface;
use Illuminate\Http\Response;

class BookService
{
    /**
     * @var BookRepositoryInterface
     */
    protected $bookRepository;

    /**
     * BookService constructor.
     * @param  BookRepositoryInterface  $bookRepository
     */
    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    /**
     * index with pagination
     *
     * @param $pageSize
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($pageSize)
    {
        return BookResource::collection($this->bookRepository->getPaginated($pageSize));
    }

    /**
     * save new book
     *
     * @param $data
     * @return array|mixed
     */
    public function store($data)
    {
        try {
            $book = $this->bookRepository->create($data);
            $book->authors()->attach(json_decode($data['authors']));

            return new BookResource($book);

        } catch (\Exception $e) {
            return [
                'error' => __('errors.book.save_error')
            ];
        }
    }

    /**
     * @param $bookID
     * @return BookResource
     */
    public function show($bookID)
    {
        return new BookResource($this->bookRepository->getById($bookID));
    }

    /**
     * Update the specified resource in storage.
     * @param $book
     * @param $bookID
     * @return array|bool|\Illuminate\Http\JsonResponse
     */
    public function update($data, $bookID)
    {
        try {
            $book = $this->bookRepository->getById($bookID);
            $book->update($data, ['id' => $bookID]);
            $book->authors()->sync(json_decode($data['authors']));
            return true;
        } catch (\Exception $e) {
            return [
                'error' => __('errors.book.update_error')
            ];
        }
    }

    /**
     * delete book
     *
     * @param $bookID
     * @return array|mixed
     */
    public function delete($bookID)
    {
        try {
            return $this->bookRepository->delete(['id' => $bookID]);
        } catch (\Exception $e) {
            return [
                'error' => __('errors.book.delete_error')
            ];
        }

    }
}
