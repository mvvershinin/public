<?php


namespace App\Services;

use App\Http\Resources\AuthorResource;
use App\Repositories\Contracts\AuthorRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AuthorService
{

    protected $authorRepository;


    public function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * index with pagination
     *
     * @param $pageSize
     * @return AnonymousResourceCollection
     */
    public function index($pageSize)
    {
        return AuthorResource::collection($this->authorRepository->getPaginated($pageSize));
    }

    /**
     * save new book
     *
     * @param $data
     * @return array|mixed
     */
    public function store($data)
    {
        try {
            return new AuthorResource($this->authorRepository->create($data));
        } catch (\Exception $e) {
            return [
                'error' => __('errors.author.save_error')
            ];
        }

    }

    /**
     * @param $authorID
     * @return AuthorResource
     */
    public function show($authorID)
    {
        return new AuthorResource($this->authorRepository->getById($authorID));
    }

    /**
     * Update the specified resource in storage.
     * @param $book
     * @param $bookID
     * @return array|bool|JsonResponse
     */
    public function update($data, $bookID)
    {
        try {
            $this->authorRepository->update($data, ['id' => $bookID]);
            return true;
        } catch (\Exception $e) {
            return [
                'error' => __('errors.author.update_error')
            ];
        }
    }

    /**
     * delete
     *
     * @param $authorID
     * @return array|mixed
     */
    public function delete($authorID)
    {
        try {
            return $this->authorRepository->delete(['id' => $authorID]);
        } catch (\Exception $e) {
            return [
                'error' => __('errors.author.delete_error')
            ];
        }

    }
}
