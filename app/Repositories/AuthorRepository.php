<?php


namespace App\Repositories;


use App\Models\Author;
use App\Models\Book;
use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Repositories\Contracts\BookRepositoryInterface;

class AuthorRepository extends BaseRepository  implements AuthorRepositoryInterface
{
    /**
     * @var
     */
    protected $model;

    protected $relations;

    /**
     * @var array
     */
    protected $defaultSelect = [
        'id',
        'name',
        'birthday',
        'created_at'
    ];

    /**
     * ItemsRepository constructor
     *
     * @param Author $model
     */
    public function __construct(Author $model)
    {
        parent::__construct($model);
        $this->relations = 'books';
    }
}
