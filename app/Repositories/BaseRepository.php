<?php

namespace App\Repositories;

use App\Repositories\Contracts\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements BaseRepositoryInterface
{
	/**
	 * @var Model
	 */
	protected $model;

    /**
     * @var array
     */
	protected $relations;

	/**
	 * @var bool
	 */
	protected $withTrashed;

    /**
     * BaseRepository constructor.
     *
     * @param  Model  $model
     */
	public function __construct(Model $model)
	{
		$this->model = $model;
		$this->relations = [];
	}

	/**
	 * @return false|string
	 */
	public function getClassName()
	{
		return get_class($this->model);
	}

	/**
	 * @return Model
	 */
	public function getInstance()
	{
		return $this->model;
	}

	/**
	 * @param bool $boolean
	 * @return $this
	 */
	public function withTrashed($boolean = false)
	{
		$this->withTrashed = $boolean;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTableName()
	{
		return $this->model->getTable();
	}

	/**
	 * @param array $attributes
	 *
	 * @return Model
	 */
	public function create(array $attributes)
	{
		return $this->model
			->create($attributes);
	}

	/**
	 * @param $id
	 * @return Model
	 */
	public function find($id)
	{
		return $this->model
			->find($id);
	}

    /**
     * Get model by ID
     *
     * @param int $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->getWithRelations()
            ->find($id);
    }

    /**
     * Get paginated with relations
     *
     * @param  int  $pageSize
     * @return mixed
     */
    public function getPaginated($pageSize)
    {
        return $this->getWithRelations()
            ->paginate($pageSize);
    }

    /**
     * pattern items with tags
     *
     * @return mixed
     */
    protected function getWithRelations()
    {
        return $this->model->with($this->relations)
            ->select($this->defaultSelect);
    }

	/**
	 * @param array $attributes
	 * @param array $where
	 * @return mixed
	 */
	public function update(array $attributes, array $where = [])
	{
		$query = $this->withTrashed ? $this->model->withTrashed() : $this->model;

		return $query
			->where($where)
			->update($attributes);
	}

	/**
	 * @param array $where
	 * @param array $attributes
	 * @return mixed
	 */
	public function updateOrCreate(array $where, array $attributes) {
		return $this->model
			->updateOrCreate($where, $attributes);
	}

	/**
	 * @param array $where
	 * @return mixed
	 */
	public function delete(array $where)
	{
		return $this->model
			->where($where)
			->delete();
	}

	/**
	 * @param string $key
	 * @param array $ids
	 * @return mixed
	 */
	public function destroy(string $key, array $ids)
	{
		return $this->model
			->whereIn($key, $ids)
			->delete();
	}

	/**
	 * @param array $where
	 * @return mixed
	 */
	public function forceDelete(array $where)
	{
		return $this->model
			->where($where)
			->forceDelete();
	}

	/**
	 * @param string $key
	 * @param array $ids
	 * @return mixed
	 */
	public function forceDestroy(string $key, array $ids)
	{
		return $this->model
			->whereIn($key, $ids)
			->forceDelete();
	}

	/**
	 * @param array $where
	 * @return mixed
	 */
	public function exists(array $where)
	{
		return $this->model
			->where($where)
			->exists();
	}
}
