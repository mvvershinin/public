<?php


namespace App\Repositories;


use App\Models\Book;
use App\Repositories\Contracts\BookRepositoryInterface;

class BookRepository extends BaseRepository  implements BookRepositoryInterface
{
    /**
     * @var
     */
    protected $model;

    protected $relations;

    /**
     * @var array
     */
    protected $defaultSelect = [
        'id',
        'title',
        'description',
        'created_at'
    ];

    /**
     * ItemsRepository constructor
     *
     * @param Book $model
     */
    public function __construct(Book $model)
    {
        parent::__construct($model);
        $this->relations = 'authors';
    }
}
