<?php

namespace App\Repositories\Contracts;

interface BaseRepositoryInterface
{
	/**
	 * @param array $attributes
	 * @return mixed
	 */
	public function create(array $attributes);

	/**
	 * @param $id
	 * @return mixed
	 */
	public function find($id);

	/**
	 * @param array $attributes
	 * @param array $where
	 * @return mixed
	 */
	public function update(array $attributes, array $where = []);

	/**
	 * @param array $where
	 * @return mixed
	 */
	public function delete(array $where);

	/**
	 * @param array $where
	 * @return mixed
	 */
	public function forceDelete(array $where);

	/**
	 * @param array $where
	 * @return mixed
	 */
	public function exists(array $where);

	/**
	 * @param array $where
	 * @param array $attributes
	 * @return mixed
	 */
	public function updateOrCreate(array $where, array $attributes);

    /**
     * Get model by ID
     *
     * @param int $id
     * @return mixed
     */
    public function getById($id);

    /**
     * get paginated
     *
     * @param $pageSize
     * @return mixed
     */
    public function getPaginated($pageSize);
}
