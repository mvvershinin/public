<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequest;
use App\Services\BookService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class BookController extends Controller
{
    const PAGE_SIZE = 25;

    /**
     * @var BookService
     */
    protected $bookService;

    /**
     * BookController constructor.
     * @param  BookService  $bookService
     */
    public function __construct(BookService $bookService)
    {
        //todo for auth
        //$this->middleware('auth:api')->except(['index', 'show']);
        $this->bookService = $bookService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return
            response()->json(
                $this->bookService->index(self::PAGE_SIZE),
                200
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BookRequest  $request
     * @return Response
     */
    public function store(BookRequest $request)
    {
        return response()->json(
            $this->bookService->store($request->validated()),
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param $bookID
     * @return Response
     */
    public function show($bookID)
    {
        return response()->json(
            $this->bookService->show($bookID),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BookRequest  $request
     * @param $bookID
     * @return Response
     */
    public function update(BookRequest $request, $bookID)
    {
        return response()->json(
            $this->bookService->update($request->validated(), $bookID),
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $bookID
     * @return Response
     */
    public function destroy($bookID)
    {
        return response()->json(
            $this->bookService->delete($bookID),
            200
        );
    }
}
