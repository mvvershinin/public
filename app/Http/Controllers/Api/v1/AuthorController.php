<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthorRequest;
use App\Services\AuthorService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    const PAGE_SIZE = 25;

    /**
     * @var AuthorService
     */
    protected $authorService;

    public function __construct(AuthorService $authorService)
    {
        //todo for auth
        //$this->middleware('auth:api')->except(['index', 'show']);
        $this->authorService = $authorService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return
            response()->json(
                $this->authorService->index(self::PAGE_SIZE),
                200
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(AuthorRequest $request)
    {
        return
            response()->json(
                $this->authorService->store($request->validated()),
                200
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($authorID)
    {
        return response()->json(
            $this->authorService->show($authorID),
            200
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param $authorID
     * @return Response
     */
    public function update(AuthorRequest $request, $authorID)
    {
        return response()->json(
            $this->authorService->update($request->validated(), $authorID),
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $authorID
     * @return Response
     */
    public function destroy($authorID)
    {
        return response()->json(
            $this->authorService->delete($authorID),
            200
        );
    }
}
