<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'title' => 'string:200',
                    'description' => 'string',
                    'authors' => 'json',
                ];
                break;
            case 'POST':
                $rules = [
                    'title' => 'required|string:200',
                    'description' => 'required|string',
                    'authors' => 'required|json',
                ];
                break;
        }
        return $rules;
    }
}
